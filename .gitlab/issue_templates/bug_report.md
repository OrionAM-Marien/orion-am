# Printer serial number
_Please specify the serial number of your printer here_

# Current behaviour
_Please specify briefly what the behaviour is that prompted you to submit this report_

# Steps to reproduce
_What did you do to get the behaviour mentioned?_

# Desired behaviour
_What behaviour do you expect or desire?_

# Additional information
_Any other information you'd like to add?_
